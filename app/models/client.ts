import { Injectable } from '@angular/core';
import { Model } from './model';

@Injectable()
export class Client extends Model {
    private _name: string;
    private _age: number;
    private _email: string;

    constructor() {
        super();
    }

    public set Name(value: string) {
        this._name = value;
    }

    public set Age(value: number) {
        this._age = value;
    }

    public set Email(value: string) {
        this._email = value;
    }

    public printProperties(): void {
        console.log(this.Id, this._name, this._age, this._email);
    }
}