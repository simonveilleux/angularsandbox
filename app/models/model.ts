import { Injectable } from '@angular/core';

@Injectable()
export class Model {
    private _id: number;

    public get Id() {
        return this._id;
    }

    public set Id(value: number) {
        this._id = value;
    } 
}