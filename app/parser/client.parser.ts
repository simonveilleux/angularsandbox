import { Injectable } from '@angular/core';
import { GenericParser } from './generic.parser';
import { Client } from './../models/client';

@Injectable()
export class ClientParser extends GenericParser<Client> {
    public parse(modelType: any, data: Object): Client {
        var client = super.parse(modelType, data);

        if(data['name']) {
            client.Name = data['name'];
        }
        if(data['email']) {
            client.Email = data['email'];
        }
        if(data['age']) {
            client.Age = data['age'];
        }

        return client;
    }
}