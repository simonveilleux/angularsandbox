import { Injectable } from '@angular/core';
import { Model } from './../models/model';

@Injectable()
export abstract class GenericParser<T extends Model> {
    public fromJson(modelType: T, json: string): T {
        var obj: Object = JSON.parse(json);

        return this.parse(modelType, obj);
    };

    // Malheureusement pas possible de faire un new avec T, donc any.
    protected parse(modelType: any, data: Object): T {
        var instance = new modelType();

        if(data['id']) {
            instance.Id = parseInt(data['id']);
        }

        return instance;
    }
}
