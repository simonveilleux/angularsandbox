import { Injector, Injectable } from '@angular/core';
import { ClientParser } from './../parser/client.parser';
import { Client } from './../models/client';

@Injectable()
export class MonService {
    constructor(injector: Injector) {
        // Injection d'une nouvelle instance dans un service.
        var parser: ClientParser = injector.get(ClientParser);

        // Malheureusement, Client return 'typeof Client', donc une string. Il faut utiliser
        // <any> pour forcer l'object a être converti en "function?".
        var client = parser.fromJson(<any>Client, JSON.stringify({
            id: 123,
            name: 'Simon Veilleux',
            email: 'sveilleux1@gmail.com',
            age: 32
        }));

        client.printProperties();
    }
}