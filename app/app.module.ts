import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { MonService } from './services/mon.service';
import { ClientParser } from './parser/client.parser';

@NgModule({
    // imports makes the exported declarations of other modules available in the current module
    imports: [BrowserModule],

    // declarations is used to declare components, directives, pipes that belongs to the current module.
    // Everything inside declarations knows each other.
    declarations: [
        AppComponent,
    ],

    // providers are to make services and values known to DI. They are added to the root scope and
    // they are injected to other services or directives that have them as dependency.
    providers: [
        MonService,
        ClientParser
    ],

    // Usually the main component of the application.
    bootstrap: [AppComponent],
})
export class AppModule { }